package com.destroystokyo.paper.loottable;

import net.minecraft.server.v1_12_R1.World;

interface CraftLootable extends Lootable {

    World getNMSWorld();

    default org.bukkit.World getBukkitWorld() {
        return getNMSWorld().getWorld();
    }
}
