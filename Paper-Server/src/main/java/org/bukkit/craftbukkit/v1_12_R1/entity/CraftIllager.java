package org.bukkit.craftbukkit.v1_12_R1.entity;

import net.minecraft.server.v1_12_R1.EntityIllagerAbstract;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.entity.Illager;

public class CraftIllager extends CraftMonster implements Illager {

    public CraftIllager(CraftServer server, EntityIllagerAbstract entity) {
        super(server, entity);
    }

    @Override
    public EntityIllagerAbstract getHandle() {
        return (EntityIllagerAbstract) super.getHandle();
    }

    @Override
    public String toString() {
        return "CraftIllager";
    }
}
