package org.bukkit.craftbukkit.v1_12_R1.command;

import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.ICommandListener;
import org.bukkit.craftbukkit.v1_12_R1.util.CraftChatMessage;

public class CraftFunctionCommandSender extends ServerCommandSender {

    private final ICommandListener handle;

    public CraftFunctionCommandSender(ICommandListener handle) {
        this.handle = handle;
    }

    @Override
    public void sendMessage(String message) {
        for (IChatBaseComponent component : CraftChatMessage.fromString(message)) {
            handle.sendMessage(component);
        }
    }

    @Override
    public void sendMessage(String[] messages) {
        for (String message : messages) {
            sendMessage(message);
        }
    }

    @Override
    public String getName() {
        return handle.getName();
    }

    @Override
    public boolean isOp() {
        return true;
    }

    @Override
    public void setOp(boolean value) {
        throw new UnsupportedOperationException("Cannot change operator status of server function sender");
    }

    public ICommandListener getHandle() {
        return handle;
    }
}
