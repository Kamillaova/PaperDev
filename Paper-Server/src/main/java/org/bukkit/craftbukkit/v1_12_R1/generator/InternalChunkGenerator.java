package org.bukkit.craftbukkit.v1_12_R1.generator;

import net.minecraft.server.v1_12_R1.IChunkProvider;
import org.bukkit.generator.ChunkGenerator;

// Do not implement functions to this class, add to NormalChunkGenerator
public abstract class InternalChunkGenerator extends ChunkGenerator implements net.minecraft.server.v1_12_R1.ChunkGenerator {
}
