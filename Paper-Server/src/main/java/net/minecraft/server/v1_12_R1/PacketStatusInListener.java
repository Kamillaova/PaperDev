package net.minecraft.server.v1_12_R1;

public interface PacketStatusInListener extends PacketListener {

    void a(PacketStatusInPing packetstatusinping);

    void a(PacketStatusInStart packetstatusinstart);
}
