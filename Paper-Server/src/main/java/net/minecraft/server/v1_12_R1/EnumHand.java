package net.minecraft.server.v1_12_R1;

public enum EnumHand {

    MAIN_HAND, OFF_HAND;

    private EnumHand() {}
}
