package net.minecraft.server.v1_12_R1;

public interface PacketLoginInListener extends PacketListener {

    void a(PacketLoginInStart packetlogininstart);

    void a(PacketLoginInEncryptionBegin packetlogininencryptionbegin);
}
