package net.minecraft.server.v1_12_R1;

public interface INamable {

    String getName();
}
