package net.minecraft.server.v1_12_R1;

public interface IRegistry<K, V> extends Iterable<V> {

    void a(K k0, V v0);
}
