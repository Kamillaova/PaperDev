package net.minecraft.server.v1_12_R1;

public interface ITileEntityContainer extends INamableTileEntity {

    Container createContainer(PlayerInventory playerinventory, EntityHuman entityhuman);

    String getContainerName();
}
