package net.minecraft.server.v1_12_R1;

public interface IBlockPhysics {

    boolean a(World world, BlockPosition blockposition, int i, int j);

    void doPhysics(World world, BlockPosition blockposition, Block block, BlockPosition blockposition1);
}
