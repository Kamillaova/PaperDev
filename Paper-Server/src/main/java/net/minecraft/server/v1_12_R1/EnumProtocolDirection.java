package net.minecraft.server.v1_12_R1;

public enum EnumProtocolDirection {

    SERVERBOUND, CLIENTBOUND;

    private EnumProtocolDirection() {}
}
