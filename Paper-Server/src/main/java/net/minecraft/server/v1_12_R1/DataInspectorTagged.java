package net.minecraft.server.v1_12_R1;

public abstract class DataInspectorTagged implements DataInspector {

    private final MinecraftKey a;

    public DataInspectorTagged(Class<?> oclass) {
        if (Entity.class.isAssignableFrom(oclass)) {
            // Kamillaova: add cast, fix build
            this.a = EntityTypes.getName((Class<? extends Entity>) oclass);
        } else if (TileEntity.class.isAssignableFrom(oclass)) {
            // Kamillaova: add cast, fix build
            this.a = TileEntity.a((Class<? extends TileEntity>) oclass);
        } else {
            this.a = null;
        }

    }

    public NBTTagCompound a(DataConverter dataconverter, NBTTagCompound nbttagcompound, int i) {
        if ((new MinecraftKey(nbttagcompound.getString("id"))).equals(this.a)) {
            nbttagcompound = this.b(dataconverter, nbttagcompound, i);
        }

        return nbttagcompound;
    }

    abstract NBTTagCompound b(DataConverter dataconverter, NBTTagCompound nbttagcompound, int i);
}
