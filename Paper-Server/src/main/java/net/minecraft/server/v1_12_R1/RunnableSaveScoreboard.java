package net.minecraft.server.v1_12_R1;

public class RunnableSaveScoreboard implements Runnable {

    private final PersistentBase a;

    public RunnableSaveScoreboard(PersistentBase persistentbase) {
        this.a = persistentbase;
    }

    public void run() {
        this.a.c();
    }
}
