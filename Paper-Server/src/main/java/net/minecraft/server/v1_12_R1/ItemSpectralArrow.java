package net.minecraft.server.v1_12_R1;

public class ItemSpectralArrow extends ItemArrow {

    public ItemSpectralArrow() {}

    public EntityArrow a(World world, ItemStack itemstack, EntityLiving entityliving) {
        return new EntitySpectralArrow(world, entityliving);
    }
}
