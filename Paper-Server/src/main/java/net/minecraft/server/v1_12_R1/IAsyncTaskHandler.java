package net.minecraft.server.v1_12_R1;

import com.google.common.util.concurrent.ListenableFuture;

public interface IAsyncTaskHandler {

    ListenableFuture<Object> postToMainThread(Runnable runnable);

    boolean isMainThread();
}
