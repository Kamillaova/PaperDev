package net.minecraft.server.v1_12_R1;

public enum EnumPistonReaction {

    NORMAL, DESTROY, BLOCK, IGNORE, PUSH_ONLY;

    private EnumPistonReaction() {}
}
