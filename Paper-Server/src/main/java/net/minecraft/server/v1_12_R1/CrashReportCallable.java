package net.minecraft.server.v1_12_R1;

import java.util.concurrent.Callable;

public interface CrashReportCallable<V> extends Callable<V> {}
