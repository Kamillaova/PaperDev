package net.minecraft.server.v1_12_R1;

public enum EnumRenderType {

    INVISIBLE, LIQUID, ENTITYBLOCK_ANIMATED, MODEL;

    private EnumRenderType() {}
}
