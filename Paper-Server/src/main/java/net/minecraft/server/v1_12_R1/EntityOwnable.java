package net.minecraft.server.v1_12_R1;

import java.util.UUID;
import javax.annotation.Nullable;

public interface EntityOwnable {

    @Nullable
    UUID getOwnerUUID();

    @Nullable
    Entity getOwner();
}
