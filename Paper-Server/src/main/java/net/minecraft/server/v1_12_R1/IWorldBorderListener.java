package net.minecraft.server.v1_12_R1;

public interface IWorldBorderListener {

    void a(WorldBorder worldborder, double d0);

    void a(WorldBorder worldborder, double d0, double d1, long i);

    void a(WorldBorder worldborder, double d0, double d1);

    void a(WorldBorder worldborder, int i);

    void b(WorldBorder worldborder, int i);

    void b(WorldBorder worldborder, double d0);

    void c(WorldBorder worldborder, double d0);
}
