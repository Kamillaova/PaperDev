package net.minecraft.server.v1_12_R1;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.annotation.Nullable;

public abstract class BlockDataAbstract implements IBlockData {

    private static final Joiner a = Joiner.on(',');
    private static final Function<Entry<IBlockState<?>, Comparable<?>>, String> b = new Function() {
        @Nullable
        public String a(@Nullable Entry<IBlockState<?>, Comparable<?>> entry) {
            if (entry == null) {
                return "<NULL>";
            } else {
                IBlockState iblockstate = (IBlockState) entry.getKey();

                return iblockstate.a() + "=" + this.a(iblockstate, (Comparable) entry.getValue());
            }
        }

        // Kamillaova: Change Comparable<?> to T (comparable arg), fix build
        private <T extends Comparable<T>> String a(IBlockState<T> iblockstate, T comparable) {
            return iblockstate.a(comparable);
        }

        @Nullable
        public Object apply(@Nullable Object object) {
            return this.a((Entry) object);
        }
    };

    public BlockDataAbstract() {}

    public <T extends Comparable<T>> IBlockData a(IBlockState<T> iblockstate) {
        // Kamillaova: remove cast to Object and to Comparable, fix build
        return this.set(iblockstate, a(iblockstate.c(), this.get(iblockstate)));
    }

    protected static <T> T a(Collection<T> collection, T t0) {
        Iterator iterator = collection.iterator();

        do {
            if (!iterator.hasNext()) {
                // Kamillaova: add cast to T
                return (T) iterator.next();
            }
        } while (!iterator.next().equals(t0));

        if (iterator.hasNext()) {
            // Kamillaova: add cast to T
            return (T) iterator.next();
        } else {
            return collection.iterator().next();
        }
    }

    public String toString() {
        StringBuilder stringbuilder = new StringBuilder();

        stringbuilder.append(Block.REGISTRY.b(this.getBlock()));
        if (!this.t().isEmpty()) {
            stringbuilder.append("[");
            BlockDataAbstract.a.appendTo(stringbuilder, Iterables.transform(this.t().entrySet(), BlockDataAbstract.b));
            stringbuilder.append("]");
        }

        return stringbuilder.toString();
    }
}
