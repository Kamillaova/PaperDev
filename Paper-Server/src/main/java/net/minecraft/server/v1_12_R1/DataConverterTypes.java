package net.minecraft.server.v1_12_R1;

public enum DataConverterTypes implements DataConverterType {

    LEVEL, PLAYER, CHUNK, BLOCK_ENTITY, ENTITY, ITEM_INSTANCE, OPTIONS, STRUCTURE;

    private DataConverterTypes() {}
}
