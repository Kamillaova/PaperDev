package net.minecraft.server.v1_12_R1;

public interface PacketStatusOutListener extends PacketListener {

    void a(PacketStatusOutServerInfo packetstatusoutserverinfo);

    void a(PacketStatusOutPong packetstatusoutpong);
}
