package net.minecraft.server.v1_12_R1;

import com.google.common.collect.Maps;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class InsensitiveStringMap<V> implements Map<String, V> {

    private final Map<String, V> a = Maps.newLinkedHashMap();

    public InsensitiveStringMap() {}

    public int size() {
        return this.a.size();
    }

    public boolean isEmpty() {
        return this.a.isEmpty();
    }

    public boolean containsKey(Object object) {
        return this.a.containsKey(object.toString().toLowerCase(Locale.ROOT));
    }

    public boolean containsValue(Object object) {
        return this.a.containsKey(object);
    }

    public V get(Object object) {
        return this.a.get(object.toString().toLowerCase(Locale.ROOT));
    }

    public V a(String s, V v0) {
        return this.a.put(s.toLowerCase(Locale.ROOT), v0);
    }

    public V remove(Object object) {
        return this.a.remove(object.toString().toLowerCase(Locale.ROOT));
    }

    public void putAll(Map<? extends String, ? extends V> map) {
        Iterator iterator = map.entrySet().iterator();

        while (iterator.hasNext()) {
            // Kamillaova: Fix
            // Orig: Entry entry = (Entry) iterator.next();
            Entry<? extends String, ? extends V> entry = (Entry<? extends String, ? extends V>) iterator.next();

            this.a((String) entry.getKey(), entry.getValue());
        }

    }

    public void clear() {
        this.a.clear();
    }

    public Set<String> keySet() {
        return this.a.keySet();
    }

    public Collection<V> values() {
        return this.a.values();
    }

    public Set<Entry<String, V>> entrySet() {
        return this.a.entrySet();
    }

    // Kamillaova: replace Object, Object with String, V, return type (Object) with V
    public V put(String object, V object1) {
        return this.a((String) object, object1);
    }
}
