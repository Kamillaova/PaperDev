package net.minecraft.server.v1_12_R1;

import javax.annotation.Nullable;

public interface DefinedStructureProcessor {

    @Nullable
    DefinedStructure.BlockInfo a(World world, BlockPosition blockposition, DefinedStructure.BlockInfo definedstructure_blockinfo);
}
