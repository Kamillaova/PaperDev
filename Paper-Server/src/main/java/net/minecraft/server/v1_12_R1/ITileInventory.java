package net.minecraft.server.v1_12_R1;

public interface ITileInventory extends IInventory, ITileEntityContainer {

    boolean isLocked();

    void setLock(ChestLock chestlock);

    ChestLock getLock();
}
