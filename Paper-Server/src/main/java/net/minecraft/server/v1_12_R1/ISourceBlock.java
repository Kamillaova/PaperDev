package net.minecraft.server.v1_12_R1;

public interface ISourceBlock extends ILocationSource {

    double getX();

    double getY();

    double getZ();

    BlockPosition getBlockPosition();

    IBlockData e();

    <T extends TileEntity> T getTileEntity();
}
