package net.minecraft.server.v1_12_R1;

public interface ICrafting {

    void a(Container container, NonNullList<ItemStack> nonnulllist);

    void a(Container container, int i, ItemStack itemstack);

    void setContainerData(Container container, int i, int j);

    void setContainerData(Container container, IInventory iinventory);
}
