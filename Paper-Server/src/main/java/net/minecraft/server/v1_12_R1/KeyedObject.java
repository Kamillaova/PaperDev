package net.minecraft.server.v1_12_R1;

public interface KeyedObject {
    MinecraftKey getMinecraftKey();
    default String getMinecraftKeyString() {
        return getMinecraftKey().toString();
    }
}
