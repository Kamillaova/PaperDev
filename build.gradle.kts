@file:Suppress("UnstableApiUsage", "UNUSED_VARIABLE")

plugins {
  java
  `java-library`
  `maven-publish`
}

allprojects {
  group = "dev.kamillaova.paper"
  version = "1.12.2-R0.1-SNAPSHOT"
}

subprojects {
  val libs = rootProject.libs

  apply {
    plugin("java")
    plugin("java-library")
    plugin("maven-publish")
  }

  java {
    withSourcesJar()
    withJavadocJar()

    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
  }

  tasks {
    withType<JavaCompile>().configureEach {
      options.run {
        encoding = Charsets.UTF_8.name()
        compilerArgs.addAll(listOf(
          "-parameters",
          "-Xlint:all,-serial,-processing,-classfile",
        ))
      }
    }
    withType<Javadoc>().configureEach {
      (options as StandardJavadocDocletOptions).run {
        encoding = Charsets.UTF_8.name()
        addStringOption("Xdoclint:none", "-quiet")
      }
    }
    withType<AbstractArchiveTask>().configureEach {
      isReproducibleFileOrder = true
      isPreserveFileTimestamps = false
    }
  }

  testing {
    suites {
      val test by getting(JvmTestSuite::class) {
        useJUnitJupiter(libs.versions.junit.get())

        dependencies {
          implementation(libs.junit.vintage.engine)
          implementation(libs.hamcrest)
        }
      }
    }
  }

  publishing {
    repositories {
      mavenLocal()
    }
  }
}
