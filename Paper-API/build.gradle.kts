dependencies {
  api(libs.guava)
  api(libs.fastutil)
  api(libs.slf4j.api)
  api(libs.bungeecord.chat)
  api(libs.authlib)
  api(libs.apache.commons.lang2)

  implementation(libs.json.simple)
  implementation(libs.snakeyaml)
  implementation(libs.asm)
  implementation(libs.asm.commons)
}

publishing {
  publications {
    create<MavenPublication>("maven") {
      artifactId = "paper-api"
      from(components["java"])
    }
  }
}
